#include <git2/global.h>
#include "global.hpp"

void git2::start()
{
  git_libgit2_init();
}

void git2::stop()
{
  git_libgit2_shutdown();
}
